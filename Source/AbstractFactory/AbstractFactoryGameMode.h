// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AbstractFactoryGameMode.generated.h"

UCLASS(minimalapi)
class AAbstractFactoryGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAbstractFactoryGameMode();
};



