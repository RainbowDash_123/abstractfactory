// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbstractFactory/AbstarctFactory/LootStructures.h"
#include "AbstractFactory/AbstarctFactory/Loot/Loot.h"
#include "ElvenLoot.generated.h"

/**
 * An element of elven armor
 */
UCLASS()
class ABSTRACTFACTORY_API AElvenArmor : public ALoot, public FArmorInfo //for some reason, private inheritance is not allowed in the UE, so public
{
	GENERATED_BODY()

public:
	virtual void StoreItem() override {/*Implementation*/ };
	virtual void UseItem() override {/*Implementation*/ };
	virtual void SetupWithInfo(FBaseInfo* InfoPtr) override;
};

inline void AElvenArmor::SetupWithInfo(FBaseInfo* InfoPtr)
{
	if(const auto ArmorInfo = static_cast<FArmorInfo*>(InfoPtr))
	{
		Durability = ArmorInfo->Durability;
	}
}

/**
 * An element of elven weapon
 */
UCLASS()
class ABSTRACTFACTORY_API AElvenWeapon : public ALoot, public FWeaponInfo
{
	GENERATED_BODY()

public:
	virtual void StoreItem() override {/*Implementation*/ };
	virtual void UseItem() override {/*Implementation*/ };
	virtual void SetupWithInfo(FBaseInfo* InfoPtr) override;
};

inline void AElvenWeapon::SetupWithInfo(FBaseInfo* InfoPtr)
{
	if (const auto WeaponInfo = static_cast<FWeaponInfo*>(InfoPtr))
	{
		Damage = WeaponInfo->Damage;
		Durability = WeaponInfo->Durability;
	}
}