// Copyright Epic Games, Inc. All Rights Reserved.

#include "AbstractFactory.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, AbstractFactory, "AbstractFactory" );
 