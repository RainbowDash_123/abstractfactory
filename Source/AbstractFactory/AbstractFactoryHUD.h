// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "AbstractFactoryHUD.generated.h"

UCLASS()
class AAbstractFactoryHUD : public AHUD
{
	GENERATED_BODY()

public:
	AAbstractFactoryHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

